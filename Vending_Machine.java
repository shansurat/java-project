import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class Vending_Machine extends JFrame implements ActionListener {
    ButtonGroup radio_buttons = new ButtonGroup();
    JTextField txtField;
    JTextArea textarea;

    String welcome_details = "                                          Welcome\n\n"
            + "Price: Orange - 1.50 Apple - 1.80 Strawberry - 2.20\n\n" + "Step 1: Choose your flavor\n"
            + "Step 2: Input your payment\n\n";

    double orange_price = 1.50;
    double apple_price = 1.80;
    double sb_price = 2.20;

    public Vending_Machine() {
        super("Vending Machine");
        Container content = getContentPane();
        content.setLayout(new BorderLayout());

        // North Panel
        JPanel NorthPanel = new JPanel();
        content.add(NorthPanel, BorderLayout.NORTH);
        JLabel label = new JLabel("Flavour");
        JRadioButton rdButton1 = new JRadioButton("Orange");
        rdButton1.setActionCommand("Orange");
        JRadioButton rdButton2 = new JRadioButton("Apple");
        rdButton2.setActionCommand("Apple");
        JRadioButton rdButton3 = new JRadioButton("Strawberry");
        rdButton3.setActionCommand("Strawberry");

        radio_buttons.add(rdButton1);
        rdButton1.setSelected(true);
        radio_buttons.add(rdButton2);
        radio_buttons.add(rdButton3);

        NorthPanel.setLayout(new FlowLayout());
        NorthPanel.add(label);
        NorthPanel.add(rdButton1);
        NorthPanel.add(rdButton2);
        NorthPanel.add(rdButton3);

        // West Panel
        JPanel WestPanel = new JPanel();
        content.add(WestPanel, BorderLayout.WEST);
        JLabel label2 = new JLabel("Payment");
        txtField = new JTextField("2.50", 10);

        WestPanel.add(label2);
        WestPanel.add(txtField);

        // Center Panel

        JPanel CenterPanel = new JPanel();
        content.add(CenterPanel, BorderLayout.CENTER);
        textarea = new JTextArea();
        textarea.setLineWrap(true);
        textarea.setWrapStyleWord(true);
        textarea.setAlignmentX(CENTER_ALIGNMENT);
        textarea.setAlignmentY(CENTER_ALIGNMENT);
        CenterPanel.setBackground(Color.green);

        CenterPanel.setLayout(new BorderLayout());
        CenterPanel.add(textarea);

        // South Panel
        JPanel SouthPanel = new JPanel();
        content.add(SouthPanel, BorderLayout.SOUTH);
        JButton btn1 = new JButton("Confirm");
        JButton btn2 = new JButton("Cancel");
        SouthPanel.setLayout(new FlowLayout());
        SouthPanel.add(btn1);
        SouthPanel.add(btn2);

        btn1.addActionListener(this);
        btn2.addActionListener(this);

        setSize(500, 400);
        setVisible(true);

        textarea.setText(welcome_details);

    }

    public String calc() {
        double price = 0;
        String flavor = radio_buttons.getSelection().getActionCommand();
        double payment = Double.parseDouble(txtField.getText());

        if (flavor == "Orange")
            price = orange_price;
        else if (flavor == "Apple")
            price = apple_price;
        else if (flavor == "Strawberry")
            price = sb_price;

        return "Flavour: " + radio_buttons.getSelection().getActionCommand() + '\n' + "Price: " + String.format("%.2f" ,price) + '\n'
                + "Payment: " + txtField.getText() + '\n' + "Change: " + String.format("%.2f", payment - price);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String command = evt.getActionCommand();

        if (command == "Confirm") {
            textarea.setText(welcome_details + calc());
        }
    }

    public static void main(String[] args) {
        Vending_Machine VM = new Vending_Machine();
        VM.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}