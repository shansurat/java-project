package CustomerBill;

public class CustomerBill {
    private String acctNum;
    private String custType;
    private int duration;
    private int dayDuration;
    private int nightDuration;

    public CustomerBill(String acctNum, String custType, int duration, int dayDuration, int nightDuration) {
        this.acctNum = acctNum;
        this.custType = custType;
        this.duration = duration;
        this.dayDuration = dayDuration;
        this.nightDuration = nightDuration;
    }

    public double calcRegularBill() {
        if (duration <= 0) return 0; 

        return (duration < 50) ? 10 : 10 + ((duration - 50) * 0.2);
    }

    public double calcPremiumBill() {
        if (dayDuration <= 0 && nightDuration <= 0) return 0;  

        return 25 + ((dayDuration < 75) ? 0 : (dayDuration - 75) * 0.1) // Day Bill
                + ((nightDuration < 75) ? 0 : (nightDuration - 75) * 0.5); // Night Bill
    }

    public String getBillInfo() {
        String billInfo = "Billing Info:\n" + "Account Number: " + acctNum + "\n" + "Service Type: " + custType
                + "\nBill Amount: ";
        double charge = 0.0;
        if (custType.equals("Regular")) {
            charge = calcRegularBill();
        } else if (custType.equals("Premium")) {
            charge = calcPremiumBill();
        } else {
            charge = 0;
        }
        billInfo = billInfo + "RM" + charge + "\n";

        return billInfo;
    }

    public static void main(String[] args) {

        CustomerBill CB = new CustomerBill("John Doe", "Regular", 175, 75, 100);
        System.out.println(CB.getBillInfo());
    }

}