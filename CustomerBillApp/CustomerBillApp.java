package CustomerBillApp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import CustomerBill.CustomerBill;

@SuppressWarnings("serial")

public class CustomerBillApp extends JFrame implements ActionListener {

    JTextField acctNumTF;
    ButtonGroup radio_buttons;
    JTextField durationTF;
    JTextField dayDurationTF;
    JTextField nightDurationTF;
    JTextArea outputTA;

    public CustomerBillApp() {
        super("Customer Bill Application");
        Container content = getContentPane();
        content.setLayout(new BorderLayout());

        // TOP PANEL
        JPanel TopPanel = new JPanel();
        TopPanel.setLayout(new FlowLayout());
        content.add(TopPanel, BorderLayout.PAGE_START);
        JLabel acctNumTFLabel = new JLabel("Account no.");
        acctNumTF = new JTextField("", 6);

        TopPanel.add(acctNumTFLabel);
        TopPanel.add(acctNumTF);

        // LEFT MIDDLE PANEL
        JPanel LeftMiddlePanel = new JPanel();
        LeftMiddlePanel.setLayout(new BoxLayout(LeftMiddlePanel, BoxLayout.Y_AXIS));

        JPanel grpRegular = new JPanel();
        grpRegular.setLayout(new FlowLayout());

        JPanel grpDuration = new JPanel();
        grpDuration.setLayout(new FlowLayout());

        content.add(LeftMiddlePanel, BorderLayout.LINE_START);

        JLabel regularRBLabel = new JLabel("Regular");
        JRadioButton regularRB = new JRadioButton();
        JLabel durationTFLabel = new JLabel("Call Duration (min) ");
        durationTF = new JTextField("0", 4);

        grpRegular.add(regularRB);
        grpRegular.add(regularRBLabel);
        grpDuration.add(durationTFLabel);
        grpDuration.add(durationTF);

        LeftMiddlePanel.add(grpRegular);
        LeftMiddlePanel.add(grpDuration);

        // RIGHT MIDDLE PANEL
        JPanel RightMiddlePanel = new JPanel();
        RightMiddlePanel.setLayout(new BoxLayout(RightMiddlePanel, BoxLayout.Y_AXIS));
        content.add(RightMiddlePanel, BorderLayout.LINE_END);

        JPanel grpPremium = new JPanel();
        grpPremium.setLayout(new FlowLayout());

        JPanel grpDayDuration = new JPanel();
        grpDayDuration.setLayout(new FlowLayout());

        JPanel grpNightDuration = new JPanel();
        grpNightDuration.setLayout(new FlowLayout());

        JLabel premiumRBLabel = new JLabel("Premium");
        JRadioButton premiumRB = new JRadioButton();
        JLabel dayDurationTFLabel = new JLabel("Day Call Duration (min) ");
        dayDurationTF = new JTextField("0", 4);
        JLabel nightDurationTFLabel = new JLabel("Night Call Duration (min) ");
        nightDurationTF = new JTextField("0", 4);

        grpPremium.add(premiumRB);
        grpPremium.add(premiumRBLabel);
        grpDayDuration.add(dayDurationTFLabel);
        grpDayDuration.add(dayDurationTF);
        grpNightDuration.add(nightDurationTFLabel);
        grpNightDuration.add(nightDurationTF);

        RightMiddlePanel.add(grpPremium);
        RightMiddlePanel.add(grpDayDuration);
        RightMiddlePanel.add(grpNightDuration);

        // BOTTOM PANEL
        JPanel BottomPanel = new JPanel();
        content.add(BottomPanel, BorderLayout.PAGE_END);

        BottomPanel.setLayout(new BoxLayout(BottomPanel, BoxLayout.Y_AXIS));
        JButton calcBTN = new JButton("Calculate");
        calcBTN.addActionListener(this);
        calcBTN.setAlignmentX(CENTER_ALIGNMENT);
        outputTA = new JTextArea();
        outputTA.setRows(5);
        outputTA.setLineWrap(true);
        outputTA.setWrapStyleWord(true);

        BottomPanel.add(calcBTN);
        BottomPanel.add(outputTA);

        regularRB.setActionCommand("Regular");
        premiumRB.setActionCommand("Premium");
        radio_buttons = new ButtonGroup();
        radio_buttons.add(regularRB);
        radio_buttons.add(premiumRB);
        regularRB.setSelected(true);

        setSize((int) (preferredSize().width * 1.5), (int) (preferredSize().height * 1.5));
        setVisible(true);

    }

    private void calcBTNActionPerformed(ActionEvent evt) {
        
        CustomerBill CB = new CustomerBill(acctNumTF.getText(), radio_buttons.getSelection().getActionCommand(),
                Integer.parseInt(durationTF.getText()), Integer.parseInt(dayDurationTF.getText()),
                Integer.parseInt(nightDurationTF.getText()));
        System.out.println(CB.getBillInfo());
        outputTA.setText(CB.getBillInfo());

    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String command = evt.getActionCommand();

        if (command == "Calculate") {
            this.calcBTNActionPerformed(evt);
        }

    }

    public static void main(String[] args) {
        CustomerBillApp CBA = new CustomerBillApp();
        CBA.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
